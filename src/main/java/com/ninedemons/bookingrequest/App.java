package com.ninedemons.bookingrequest;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Inbox;
import akka.actor.Terminated;
import com.ninedemons.bookingrequest.actors.CannedBookings;
import com.ninedemons.bookingrequest.actors.MeetingRoom;
import com.ninedemons.bookingrequest.actors.PrintBookings;
import com.ninedemons.bookingrequest.actors.messages.ListBookings;
import com.ninedemons.bookingrequest.domain.RoomBooking;
import scala.concurrent.duration.Duration;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Hello world!
 */
public class App {

    private final ActorSystem system;
    private ActorRef meetingroom;
    private final Inbox inbox;

    private List<RoomBooking> requests;


    public App() {
        // Create the 'meetingsbooking' actor system
        system = ActorSystem.create("meetings");

        // Create the 'meeting room' actor
        meetingroom = system.actorOf(MeetingRoom.props(9, 17), "room");

        // Create the "actor-in-a-box"
        inbox = Inbox.create(system);

    }


    public void run() throws InterruptedException {


        requestBookings();
        listBookings();


        system.shutdown();
        system.awaitTermination();

    }


    /**
     * Uses the print bookings actor to dump out all bookings in the system, and then
     * block until the actor terminates.
     */
    private void listBookings() {


        ActorRef bookingsPrinter = system.actorOf(PrintBookings.props(meetingroom), "PrintBookings");

        inbox.watch(bookingsPrinter);

        inbox.send(bookingsPrinter,new ListBookings());
        Object response = inbox.receive(Duration.create(1, TimeUnit.SECONDS));

        assert response instanceof Terminated;
        Terminated terminated = (Terminated) response;
        assert terminated.getActor().equals(bookingsPrinter);

    }

    /**
     * Uses the canned booking actor to send in test bookings into the system, and then
     * block until the actor terminates.
     */
    private void requestBookings()  {

        ActorRef bookingGenerator = system.actorOf(CannedBookings.props(meetingroom), "CannedBookings");

        inbox.watch(bookingGenerator);

        inbox.send(bookingGenerator,new CannedBookings.SendBookings());
        Object response = inbox.receive(Duration.create(1, TimeUnit.SECONDS));

        assert response instanceof Terminated;
        Terminated terminated = (Terminated) response;
        assert terminated.getActor().equals(bookingGenerator);


    }


    public static void main(String[] args) throws InterruptedException {

        App app = new App();
        app.run();


    }
}
