package com.ninedemons.bookingrequest.actors;

import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import com.ninedemons.bookingrequest.actors.messages.BookingAccepted;
import com.ninedemons.bookingrequest.actors.messages.BookingRejected;
import com.ninedemons.bookingrequest.actors.messages.BookingsForADay;
import com.ninedemons.bookingrequest.actors.messages.ListBookings;
import com.ninedemons.bookingrequest.domain.RoomBooking;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.TreeSet;

/**
 * Bookings for a given date
 */
public class BookingsForDate extends AbstractActor {


    TreeSet<RoomBooking> bookings = new TreeSet<>(new Comparator<RoomBooking>() {
        @Override
        public int compare(RoomBooking o1, RoomBooking o2) {
            return o1.getStartHour().compareTo(o2.getStartHour());
        }

    });

    public BookingsForDate(LocalDate date) {

        log.debug("Creating a bookings actor for {}",date);

        receive(ReceiveBuilder.
                match(RoomBooking.class, roomBooking -> {


                    boolean roomFree = ! bookings.stream().anyMatch(booking -> booking.overlaps(roomBooking));

                    if (roomFree) {
                        bookings.add(roomBooking);
                        sender().tell(new BookingAccepted(roomBooking), self());
                    } else {
                        sender().tell(new BookingRejected("Booking overlaps a reservation",roomBooking),self());
                    }

                }).
                match(ListBookings.class, listBookings ->{
                        sender().tell(new BookingsForADay(date,bookings),self());
                }).
                matchAny(o -> log.info("received unknown message")).build());
    }

    public static Props props(LocalDate date) {
        // You need to specify the actual type of the returned actor
        // since Java 8 lambdas have some runtime type information erased
        return Props.create(BookingsForDate.class, () -> new BookingsForDate(date));
    }

    private final LoggingAdapter log = Logging.getLogger(context().system(), this);
}
