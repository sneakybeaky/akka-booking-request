package com.ninedemons.bookingrequest.actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import com.ninedemons.bookingrequest.actors.messages.BookingAccepted;
import com.ninedemons.bookingrequest.actors.messages.BookingRejected;
import com.ninedemons.bookingrequest.domain.RoomBooking;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Actor that sends the canned data from the brief & then dies
 */
public class CannedBookings extends AbstractActor {

    private final LoggingAdapter log = Logging.getLogger(context().system(), this);

    private int acksRemaining = 0;

    public CannedBookings(ActorRef meetingRoom) {

        receive(ReceiveBuilder.
                match(SendBookings.class, SendBookings -> {

                    for (RoomBooking booking : readRequests()) {
                        meetingRoom.tell(booking, self());
                        acksRemaining++;
                    }

                    context().become(pendingAcks(), false);

                }).
                matchAny(o -> log.info("received unknown message")).build());
    }

    /**
     * State when all requests sent.
     * Wait for acks and die when all processed.
     *
     * @return a receive block for processing acks
     */
    public PartialFunction<Object, BoxedUnit> pendingAcks() {
        return ReceiveBuilder.
                match(BookingAccepted.class, accepted -> {
                    log.debug("Booking for {} accepted", accepted.getBooking());
                    processAck();

                }).
                match(BookingRejected.class, rejected -> {
                    log.debug("Booking rejected : {}", rejected);
                    processAck();
                }).
                matchAny(o -> log.info("received unknown message")).build();
    }

    private void processAck() {
        acksRemaining--;

        if (acksRemaining == 0) {
            log.debug("All acks received - terminating");
            self().tell(PoisonPill.getInstance(), self());
        }
    }


    private Collection<RoomBooking> readRequests() {
        Collection<RoomBooking> requests = new ArrayList<RoomBooking>();

        requests.add(new RoomBooking("EMP005", LocalDate.of(2011, 3, 21), 16, 3));  // 2011-03-15 17:29:12 EMP005
        requests.add(new RoomBooking("EMP002", LocalDate.of(2011, 3, 21), 9, 2));   // 2011-03-16 12:34:56 EMP002
        requests.add(new RoomBooking("EMP003", LocalDate.of(2011, 3, 22), 14, 2));  // 2011-03-16 09:28:23 EMP003
        requests.add(new RoomBooking("EMP001", LocalDate.of(2011, 3, 21), 9, 2));   // 2011-03-17 10:17:06 EMP001
        requests.add(new RoomBooking("EMP004", LocalDate.of(2011, 3, 22), 16, 1));  // 2011-03-17 10:17:06 EMP004

        return requests;

    }

    public static Props props(ActorRef meetingRoom) {
        // You need to specify the actual type of the returned actor
        // since Java 8 lambdas have some runtime type information erased
        return Props.create(CannedBookings.class, () -> new CannedBookings(meetingRoom));
    }

    public static final class SendBookings {
    }

}
