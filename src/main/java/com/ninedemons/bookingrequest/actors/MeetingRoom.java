package com.ninedemons.bookingrequest.actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.dispatch.Futures;
import akka.dispatch.Mapper;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import akka.util.Timeout;
import com.ninedemons.bookingrequest.actors.messages.AllBookings;
import com.ninedemons.bookingrequest.actors.messages.BookingRejected;
import com.ninedemons.bookingrequest.actors.messages.BookingsForADay;
import com.ninedemons.bookingrequest.actors.messages.ListBookings;
import com.ninedemons.bookingrequest.domain.RoomBooking;
import scala.Option;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static akka.pattern.Patterns.ask;
import static akka.pattern.Patterns.pipe;

/**
 * Represents a meeting room
 */
public class MeetingRoom extends AbstractActor {

    public MeetingRoom(Integer openingHour, Integer closingHour) {

        receive(ReceiveBuilder.
                match(RoomBooking.class, roomBooking -> {

                    if ((roomBooking.getStartHour() < openingHour) ||
                            (roomBooking.getStartHour() + roomBooking.getDurationInHours() > closingHour) ) {

                        log.debug("Booking outside of office hours");
                        BookingRejected rejection = new BookingRejected("Outside of office hours",roomBooking);
                        sender().tell(rejection, self());
                    } else {
                        ActorRef bookingsForDay = getBookingsForDay(roomBooking.getDate(), BookingsForDate::props);
                        bookingsForDay.forward(roomBooking,getContext());
                    }

                }).
                match(ListBookings.class, listBookings -> {

                    final ActorRef originalSender = sender();

                   Timeout timeout = new Timeout(Duration.create(5, "seconds"));
                    final ArrayList<Future<Object>> futures = new ArrayList<Future<Object>>();

                    for (ActorRef each : getContext().getChildren()) {
                        futures.add(ask(each, listBookings, timeout));
                    }

                    final Future<Iterable<Object>> aggregate = Futures.sequence(futures,
                            context().system().dispatcher());


                    final Future<AllBookings> transformed = aggregate.map(
                            new Mapper<Iterable<Object>, AllBookings>() {
                                @Override
                                public AllBookings apply(Iterable<Object> coll) {

                                    List<BookingsForADay> allBookings = new ArrayList<BookingsForADay>();

                                    for (Object response : coll) {
                                        BookingsForADay bookingsForADay = (BookingsForADay) response;
                                        allBookings.add(bookingsForADay);
                                    }

                                    return new AllBookings(allBookings);

                                }
                            }, context().system().dispatcher());


                    pipe(transformed, getContext().system().dispatcher()).to(originalSender);

                }).
                matchAny(o -> log.info("received unknown message")).build());
    }

    /**
     * Get the actor for the given day, creating it if it doesn't already exist.
     */
    private ActorRef getBookingsForDay(LocalDate date, Function<LocalDate, Props> props) {
        Option<ActorRef> maybeChild = context().child(date.toString());

        if (maybeChild.isDefined()) {
            return maybeChild.get();
        } else {
            return getContext().actorOf(props.apply(date), date.toString());
        }
    }


    public static Props props(Integer openingHour, Integer closingHour) {
        // You need to specify the actual type of the returned actor
        // since Java 8 lambdas have some runtime type information erased
        return Props.create(MeetingRoom.class, () -> new MeetingRoom(openingHour, closingHour));
    }

    private final LoggingAdapter log = Logging.getLogger(context().system(), this);
}
