package com.ninedemons.bookingrequest.actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import com.ninedemons.bookingrequest.actors.messages.AllBookings;
import com.ninedemons.bookingrequest.actors.messages.BookingsForADay;
import com.ninedemons.bookingrequest.actors.messages.ListBookings;
import com.ninedemons.bookingrequest.domain.RoomBooking;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Actor that prints bookings in the system & then dies
 */
public class PrintBookings extends AbstractActor {

    private final LoggingAdapter log = Logging.getLogger(context().system(), this);


    public PrintBookings(ActorRef meetingRoom) {

        receive(ReceiveBuilder.
                match(ListBookings.class, listBookings -> {

                    meetingRoom.tell(listBookings,self());

                }).
                match(AllBookings.class, bookings -> {
                    dumpBookings(bookings);
                    self().tell(PoisonPill.getInstance(), self());
                }).
                matchAny(o -> log.info("received unknown message")).build());
    }

    private void dumpBookings(AllBookings bookings) {
        DateTimeFormatter hourOnlyFormatter = DateTimeFormatter.ofPattern("HH:mm");

        log.info("**************");
        log.info("All bookings");
        log.info("**************");

        for (BookingsForADay bookingsForDay : bookings.getAllBookings()) {

            LocalDate date = bookingsForDay.getDate();
            log.info("{}", date);

            for (RoomBooking roomBooking : bookingsForDay.getBookings()) {
                LocalDateTime startsAt = date.atTime(roomBooking.getStartHour(),0);
                LocalDateTime finishesAt = date.atTime(roomBooking.getStartHour() + roomBooking.getDurationInHours(),0);
                log.info("{} {} {}", startsAt.format(hourOnlyFormatter), finishesAt.format(hourOnlyFormatter), roomBooking.getBookedBy());
            }

        }

    }


    public static Props props(ActorRef meetingRoom) {
        // You need to specify the actual type of the returned actor
        // since Java 8 lambdas have some runtime type information erased
        return Props.create(PrintBookings.class, () -> new PrintBookings(meetingRoom));
    }

}
