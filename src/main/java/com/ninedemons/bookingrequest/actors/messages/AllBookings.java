package com.ninedemons.bookingrequest.actors.messages;

import java.util.Collection;

/**
 * All bookings in the system
 */
public class AllBookings {

    /**
     * A collection of bookings, in ascending order of days
     */
    private final Collection<BookingsForADay> allBookings;

    public AllBookings(Collection<BookingsForADay> allBookings) {
        this.allBookings = allBookings;
    }

    public Collection<BookingsForADay> getAllBookings() {
        return allBookings;
    }
}
