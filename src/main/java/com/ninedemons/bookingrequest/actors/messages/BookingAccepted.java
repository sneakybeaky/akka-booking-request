package com.ninedemons.bookingrequest.actors.messages;

import com.ninedemons.bookingrequest.domain.RoomBooking;

/**
 * Signals that a booking has been accepted
 */
public class BookingAccepted {

    private final RoomBooking booking;

    public BookingAccepted(RoomBooking booking) {
        this.booking = booking;
    }

    public RoomBooking getBooking() {
        return booking;
    }

    @Override
    public String toString() {
        return "BookingAccepted{" +
                "booking=" + booking +
                '}';
    }
}
