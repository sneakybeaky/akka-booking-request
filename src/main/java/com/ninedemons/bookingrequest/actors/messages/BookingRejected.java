package com.ninedemons.bookingrequest.actors.messages;

import com.ninedemons.bookingrequest.domain.RoomBooking;


public class BookingRejected {

    private String reason;
    private RoomBooking booking;

    public BookingRejected(String reason, RoomBooking booking) {
        this.reason = reason;
        this.booking = booking;
    }

    public String getReason() {
        return reason;
    }

    public RoomBooking getBooking() {
        return booking;
    }

    @Override
    public String toString() {
        return "BookingRejected{" +
                "reason='" + reason + '\'' +
                ", booking=" + booking +
                '}';
    }
}
