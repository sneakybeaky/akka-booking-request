package com.ninedemons.bookingrequest.actors.messages;

import com.ninedemons.bookingrequest.domain.RoomBooking;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;

/**
 * Bookings for a given date
 */
public class BookingsForADay {

    /**
     * The date of these bookings
     */
    private LocalDate date;

    /**
     * An ordered list of bookings, in ascending hour of day
     */
    private Collection<RoomBooking> bookings;

    public BookingsForADay(LocalDate date, Collection<RoomBooking> bookings) {
        this.date = date;
        this.bookings = Collections.unmodifiableCollection(bookings);
    }

    public LocalDate getDate() {
        return date;
    }

    public Collection<RoomBooking> getBookings() {
        return bookings;
    }

    @Override
    public String toString() {
        return "Bookings{" +
                "date=" + date +
                ", bookings=" + bookings +
                '}';
    }
}
