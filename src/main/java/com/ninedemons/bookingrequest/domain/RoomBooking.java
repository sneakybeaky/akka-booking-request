package com.ninedemons.bookingrequest.domain;

import java.time.LocalDate;

/**
 * Represents a booking
 */
public class RoomBooking {

    private String bookedBy;
    private LocalDate date;

    /**
     * The hour of day the meeting starts, in 24 hour format.
     * Possible values are 0 to 23
     */
    private Integer startHour;
    private Integer durationInHours;

    public RoomBooking(String bookedBy, LocalDate date, Integer startHour, Integer durationInHours) {

        this.bookedBy = bookedBy;
        this.date = date;
        this.startHour = startHour;
        this.durationInHours = durationInHours;
    }

    public boolean overlaps(RoomBooking otherBooking) {

        boolean overlap = false;

        if (sameDay(otherBooking)) {

            overlap = startsDuring(otherBooking) ||
                    finishesDuring(otherBooking);

        }

        return overlap;
    }

    private boolean startsDuring(RoomBooking otherBooking) {
        int ourEndHour = startHour + durationInHours;
        int otherBookingEndHour = otherBooking.getStartHour() + otherBooking.getDurationInHours();

        return (otherBookingEndHour > startHour && otherBookingEndHour <= ourEndHour );
    }

    private boolean finishesDuring(RoomBooking otherBooking) {
        int ourEndHour = startHour + durationInHours;

        return (otherBooking.getStartHour() > startHour && otherBooking.getStartHour() < ourEndHour);
    }

    private boolean sameDay(RoomBooking otherBooking) {
        return this.date.equals(otherBooking.getDate());
    }

    public String getBookedBy() {
        return bookedBy;
    }

    public LocalDate getDate() {
        return date;
    }

    public Integer getStartHour() {
        return startHour;
    }

    public Integer getDurationInHours() {
        return durationInHours;
    }

    @Override
    public String toString() {
        return "RoomBooking{" +
                "bookedBy='" + bookedBy + '\'' +
                ", date=" + date +
                ", startHour=" + startHour +
                ", durationInHours=" + durationInHours +
                '}';
    }
}
