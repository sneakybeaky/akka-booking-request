package com.ninedemons.bookingrequest.actors;

import akka.actor.ActorSystem;
import akka.testkit.JavaTestKit;
import akka.testkit.TestActorRef;
import com.ninedemons.bookingrequest.actors.messages.BookingRejected;
import com.ninedemons.bookingrequest.domain.RoomBooking;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.time.LocalDate;

import static org.testng.Assert.*;

public class MeetingRoomTest {

    static ActorSystem system;

    @BeforeClass
    public static void setup() {
        system = ActorSystem.create();
    }

    @AfterClass
    public static void teardown() {
        JavaTestKit.shutdownActorSystem(system);
        system = null;
    }

    @Test
    public void requestBeforeOfficeOpens() throws Exception {
        final TestActorRef<MeetingRoom> underTest = TestActorRef.create(system,MeetingRoom.props(9,17),"testBeforeOfficeOpens");

        // Given a booking that starts before the office opens
        RoomBooking roomBooking = new RoomBooking("blah", LocalDate.now(),1,1);

        // when trying to book
        final Future<Object> future = akka.pattern.Patterns.ask(underTest, roomBooking, 3000);

        // then it will be rejected
        assertTrue(future.isCompleted());
        Object result = Await.result(future, Duration.Zero());
        assertTrue(result instanceof BookingRejected,"Booking should have been rejected");
    }

}