package com.ninedemons.bookingrequest.domain;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDate;

import static org.testng.Assert.*;

public class RoomBookingTest {


    RoomBooking underTest;

    @BeforeClass
    public void setup() {
        LocalDate localDate = LocalDate.of(2014,1,1);
        underTest = new RoomBooking("USER001",localDate,12,3);
    }


    @Test
    public void overlapsWithItself() {
        // When checked to see if a booking overlaps with itself
        // Then they do overlap
        assertTrue(underTest.overlaps(underTest),"A booking will always overlap with itself");
    }

    @Test
    public void immediatelyBefore() {
        // Given one booking finishing just before another
        RoomBooking immediatelyBefore = new RoomBooking("BLAH",underTest.getDate(),underTest.getStartHour()-1,1);

        // Then they do not overlap
        assertFalse(underTest.overlaps(immediatelyBefore),"The bookings are adjacent and do not overlap");
    }

    @Test
    public void immediatelyAfter() {
        // Given one booking starting just after another
        RoomBooking immediatelyAfter = new RoomBooking("BLAH",underTest.getDate(),underTest.getStartHour()+underTest.getDurationInHours(),1);

        // Then they do not overlap
        assertFalse(underTest.overlaps(immediatelyAfter),"The bookings are adjacent and do not overlap");
    }

    @Test
    public void differentDays() {

        LocalDate oneDayLater = underTest.getDate().plusDays(1);
        // Given a room booking at the same time but in a different day
        RoomBooking oneDayLaterRequest = new RoomBooking(underTest.getBookedBy(),oneDayLater,underTest.getStartHour(),underTest.getDurationInHours());

        // then they do not overlap
        assertFalse(underTest.overlaps(oneDayLaterRequest),"The bookings are one day apart and do not overlap");
    }


    @Test
    public void startsEarlierAndOverlaps() {

        // Given a booking that starts earlier but finishes during ours
        RoomBooking startingBefore = new RoomBooking("BLAH",underTest.getDate(),underTest.getStartHour()-1,2);

        // Then they do  overlap
        assertTrue(underTest.overlaps(startingBefore), "The earlier booking overlaps by an hour");
    }

    @Test
    public void startsDuringOurMeetingAndFinishesLater() {

        // Given a booking that starts during our meeting and finishes after
        RoomBooking startingDuring = new RoomBooking("BLAH",underTest.getDate(),underTest.getStartHour()+1,underTest.getDurationInHours());

        // Then they do  overlap
        assertTrue(underTest.overlaps(startingDuring),"The other booking starts during ours");
    }

    @Test
    public void startsLaterAndEndsEarlierThanOurMeeting() {

        // Given a booking that starts later and ends earlier than our meeting
        RoomBooking startingDuring = new RoomBooking("BLAH",underTest.getDate(),underTest.getStartHour()+1,1);
        assertTrue(startingDuring.getDurationInHours() > 0);

        // Then they do  overlap
        assertTrue(underTest.overlaps(startingDuring),"The other booking starts during ours");
    }

    @Test
    public void startsAtTheSameTimeAndEndsEarlierThanOurMeeting() {

        // Given a booking that starts at the same time and ends earlier than our meeting
        RoomBooking startingDuring = new RoomBooking("BLAH",underTest.getDate(),underTest.getStartHour(),underTest.getDurationInHours()-1);
        assertTrue(startingDuring.getDurationInHours() > 0);

        // Then they do overlap
        assertTrue(underTest.overlaps(startingDuring),"The other booking starts at the same time as ours");
    }

    @Test
    public void startsDuringAndEndsAtTheSameTime() {

        // Given a booking that starts during our meeting and finishes at the same time
        RoomBooking startingDuring = new RoomBooking("BLAH",underTest.getDate(),underTest.getStartHour()+1,underTest.getDurationInHours()-1);
        assertTrue(startingDuring.getDurationInHours() > 0);

        // Then they do overlap
        assertTrue(underTest.overlaps(startingDuring),"The other booking starts at the same time as ours");
    }

}